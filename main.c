#include <stdio.h>
#include "list.h"

int main() {
    List_T p1, p2;
    p1 = List_list(NULL);
    p2 = List_list("Atom", "Mem", "Arena", "List", NULL);
    List_free(&p1);
    List_free(&p2);
    return 0;
}