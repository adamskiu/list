#ifndef LIST_INCLUDED
#define LIST_INCLUDED

#define T List_T
typedef struct T *T;

struct T {
    T rest;
    void *first;
};

extern T      List_append (T list, T tail);
extern T      List_copy   (T list);
extern T      List_list   (void *x, ...);
extern T      List_pop    (T list, void **x);
extern T      List_push   (T list, void *x);
extern T      List_reverse(T list);
extern int    List_length (T list);
extern void   List_free   (T *list);
extern void   List_map    (T list,
    void apply(void **x, void *cl), void *cl);
extern void **List_toArray(T list, void *end);

#undef T
#endif // LIST_INCLUDED

/* Most ADTs (Abstract Data Structures)
 * hide the implementation details of their types.
 * List reveals those details because for this
 * particular ADT, the complications induced
 * by the alternatives outweigh the benefits
 * of doing so. */

/*
 * List_list creates and returns a list.
 * It's called with N nonnull pointers followed by one null pointer,
 * and it creates a list with N nodes whose first fields hold the
 * N nonnull pointers and whose Nth rest field is null.
 * For example, the assignments
 *     List_T p1, p2;
 *     p1 = List_list(NULL);
 *     p2 = List_list("Atom", "Mem", "Arena", "List", NULL);
 * return the empty list and a list with four nodes holding
 * the pointers to the strings Atom, Mem, Arena and List.
 * There's no prototype to provide the necessary implicit conversions,
 * so programmers must provide casts when passing other than char
 * pointers and void pointers as the second and subsequent arguments.
 * For example, to build a list of four one-element lists that
 * hold the strings Atom, Mem, Arena and List, the correct call is:
 * p = List_list(List_list("Atom", NULL),
 *       (void *)List_list("Mem",  NULL),
 *       (void *)List_list("Arena, NULL),
 *       (void *)List_list("List", NULL), NULL);
 *
 * List_push(T list, void *x) adds a new node that holds x to the
 * beginning of list, and returns the new list. List_push is another way
 * to create a new list; for example,
 *     p2 = List_push(NULL, "List");
 *     p2 = List_push(p2,   "Arena");
 *     p2 = List_push(p2,   "Mem");
 *     p2 = List_push(p2,   "Atom");
 * creates the same list as
 *     p2 = List_list("Atom", "Mem", "Arena", "List", NULL);
 *
 * List pop - given a nonempty list, List_pop(T list, void **x)
 * assigns the first field of the list node to *x, if x is nonnull,
 * removes and deallocates the first node, and returns the resulting list.
 * Given an empty list, List_pop simply returns it and does not change *x.
 *
 * List_append(T list, T tail) appends one list to another: It assigns tail
 * to the last rest field in list. If list is null, it returns tail.
 * Thus
 *     p2 = List_append(p2, List_list("Except", NULL));
 * sets p2 to the five element list formed by appending the one-element
 * list holding Except to the four-element list created above.
 *
 * List_reverse reverses the order of the nodes in its list argument
 * and returns resulting list. For example,
 *     p2 = List_reverse(p2);
 * returns a list that holds Except, List, Arena, Mem and Atom.
 *
 * List_copy is an applicative function: It makes and return a copy
 * of its argument. Thus after executing
 *     List_T p3 = List_reverse(List_copy(p2));
 * p3 is the list Atom, Mem, Arena and List, and Except; p2 remains unchanged.
 *
 * List_length returns the number of nodes in its argument.
 *
 * List_free takes a pointer to a T. If *list is nonnull, List_free deallocates
 * all the nodes on *list and sets it to the null pointer. If *list is null,
 * List_free has no effect.
 *
 * List_map calls the function pointed to by apply for every node in list.
 * Clients can pass an application-specific pointer, cl, to List_map,
 * and this pointer is passed along to *apply as its second argument.
 * For each node in list, *apply is called with a pointer to its first field and with cl.
 * Since *apply is called with pointers to the first fields, it can change them.
 * Taken together, apply and cl are called a closure or callback: The specify an
 * operation and some context-specific data for that operation. For example,
 * given:
 *
 *     void mkatom(void **x, void *cl) {
 *       char **str = (char **)x;
 *       FILE *fp = cl;
 *
 *       *str = Atom_string(*str);
 *       fprintf(fp, "%s\n", *str);
 *     }
 *
 * The call to List_map(p3, mkarom, stderr) replaces the strings in p3 with
 * equivalent atoms and prints
 * Atom
 * Mem
 * Arena
 * List
 * Except
 * on the error output. Another example is:
 *
 * void applyFree(void **ptr, void *cl) {
 *     FREE(*ptr);
 * }
 *
 * which can be used to deallocate the space pointed to by the first fields of a list
 * before the list itself is deallocated. For example:
 *
 * List_T names;
 * ...
 * List_map(names, applyFree, NULL);
 * List_free(&names);
 *
 * frees the data in the list names and then frees the nodes themselves.
 * It is an unchecked runtime error for apply to change list.
 *
 *
 * List_toArray(T List, void *end) - given a list with N values it creates an
 * array in which elements zero through N-1 hold the N values from
 * the first fields of the list and the Nth element holds the value of
 * end, which is often the null pointer. List_toArray returns a pointer to the
 * first element of this array. For example, the elements of p3 can be printed
 * in sorted order by
 *     int i;
 *     char **array = (char **)List_toArray(p3, NULL);
 *     qsort((void **)array, List_length(p3), sizeof(*array),
 *         (int (*)(const void *, const void *))compare);
 *     for (i = 0; array[i]; i++)
 *       printf("%s\n", array[i]);
 *     FREE(array);
 * As suggested by this example, clients must deallocate the array returned by
 * List_toArray; If the list is empty, List_toArray returns a one-element array.
 * List_toArray c
 *
 * Exceptions:
 * - List_list    can raise Mem_Failed
 * - List_push    can raise Mem_Failed
 * - List_copy    can raise Mem_Failed
 * - List_toArray can raise Mem_Failed
 *
 * Checked runtime errors:
 * - To pass a null pointer to List_free
 *
 * Unchecked runtime errors:
 * - To omit the casts to void pointer in the second and subsequent
 *   arguments in List_list()
 * - For apply to change a list
 */
