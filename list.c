#include <stdarg.h>
#include <stddef.h>
#include "assert.h"
#include "mem.h"
#include "list.h"

#define T List_T

/* Functions */

T List_push(T list, void *x) {
    T p;

    NEW(p);
    p->first = x;
    p->rest = list;
    return p;
}

/* The list creation function, List_list is complicated because
 * it must cope with a variable number of arguments and must
 * append a new node to the evolving list for each nonnull pointer argument.
 * To do so, it uses a pointer to the pointer to which the new node should be
 * assigned.
 * p starts by pointing to list(variable), so a pointer to the first node is assigned to list.
 *  Thereafter, p points to the rest field of the last node on the list.
 *  Each trip through the loop assigns the next pointer argument to x and
 *  breaks when it hits the first null-pointer argument, which might be
 *  the initial value of x. This idiom ensures that List_list(NULL)
 *  returns the empty list - a null pointer.
 *  List_list use of pointers to pointers - List_T *s - is typical of many
 *  list-manipulation algorithms. It uses one succinct mechanism to deal
 *  with two conditions: the initial node in a possibly empty list, and
 *  the interior nodes of a nonempty list. List_append illustrates another
 *  use of this idiom.*/

T List_list(void *x, ...) {
    va_list ap;
    T list, *p = &list;

    va_start(ap, x);
    for ( ; x; x = va_arg(ap, void *)) {
        NEW(*p);
        (*p)->first = x;
        p = &(*p)->rest;
    }
    *p = NULL; // last node is null
    va_end(ap);
    return list;
}


/* List_append walks p down list until it points to the null pointer
 * at the end of the list to which tail should be assigned.
 * If list itself is the null pointer, p ends uip pointing to list,
 * which has the desired effect of appending tail to the empty list. */
T List_append(T list, T tail) {
    T *p = &list;

    while(*p)
        p = &(*p)->rest;
    *p = tail;
    return list;
}

/* List_copy is the last of the List functions that uses the pointer-to-pointer idiom. */

T List_copy(T list) {
    T head, *p = &head;

    for ( ; list; list = list->rest) {
        NEW(*p);
        (*p)->first = list->first;
        p = &(*p)->rest;
    }
    *p = NULL;
    return head;
}

/* List_pop - pointers to pointers don't simplify this function, so
 * the perhaps more obvious implementation will suffice.
 * List_pop removes the first node in a nonempty list and returns the new list,
 * or simply returns an empty list. If x is nonnull, *x is assigned the contents
 * of the first field of the first node before that node is discarded.
 * Notice that List_pop must save list_.rest before deallocationg the node
 * pointed to by list. */

T List_pop(T list, void **x) {
    if (list) {
        T head = list->rest;
        if (x)
            *x = list->first;
        FREE(list);
        return head;
    } else
        return list;
}

/* List_reverse walks two pointers, list and next, down
 * the list once and uses them to reverse the list in place as it goes;
 * new always points to th efirst node of the reversed list. */

T List_reverse(T list) {
    T head = NULL, next;

    for ( ; list; list = next) {
        next = list->rest;
        list->rest = head;
        head = list;
    }
    return head;
}

/* List_length walks down list counting its nodes */
int List_length(T list) {
    int n;

    for (n = 0; list; list = list->rest)
        n++;
    return n;
}

/* List_free walks down list deallocating each node. */

void List_free(T *list) {
    T next;

    assert(list);
    for ( ; *list; *list = next) {
        next = (*list)->rest;
        FREE(*list);
    }
}

/* List_map sounds complicated, but it's trivial, because
 * the closure function dfoes all the work.
 * List_map simply walks down list calling the closure function
 * with a pointer to each node's first field and with the
 * client-specific pointer cl*/

void List_map(T list,
    void apply(void **x, void *cl),
    void *cl) {
    for ( ; list; list = list->rest)
        apply(&list->first, cl);
}

/* List_toArray allocates an N+1 element array to hold the
 * pointers in an N-element list, and copies the pointers into the
 * array. Allocating a one-element array for an empty luist may seem a waste,
 * but doing so means that List_toArray always returns a nonnull pointer
 * to an array, so clients never need to check for null pointers. */

void **List_toArray(T list, void *end) {
    int i, n = List_length(list);
    void **array = ALLOC((n +1) * sizeof(*array));

    for (i = 0; i < n; i++) {
        array[i] = list->first;
        list = list->rest;
    }
    array[i] = end;
    return array;
}