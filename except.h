#ifndef EXCEPT_INCLUDED
#define EXCEPT_INCLUDED
#include <setjmp.h>

#define T Except_T
typedef struct T {
    const char *reason;
}T;

typedef struct Except_Frame Except_Frame;
struct Except_Frame {
    Except_Frame *prev;
    jmp_buf env;
    const char *file;
    int line;
    const T *exception;
};

enum { Except_entered = 0, Except_raised,
       Except_handled,     Except_finalized };

extern Except_Frame *Except_stack;

void Except_raise(const T *e, const char *file, int line);

#define RAISE(e) Except_raise(&(e), __FILE__, __LINE__)

#define RERAISE Except_raise(Except_frame.exception, \
                             Except_frame.file,      \
                             Except_frame.line)

#define RETURN switch (Except_stack = Except_stack->prev, 0) default : return

#define TRY do { \
    volatile int Except_flag; \
    Except_Frame Except_frame; \
    Except_frame.prev = Except_stack; \
    Except_stack = &Except_frame; \
    Except_flag = setjmp(Except_frame.env); \
    if (Except_flag == Except_entered) {

#define EXCEPT(e) \
if (Except_flag == Except_entered) Except_stack = Except_stack->prev; \
}else if (Except_frame.exception == &(e)) { \
   Except_flag = Except_handled;

#define ELSE \
        if (Except_flag == Except_entered) Except_stack = Except_stack->prev; \
        } else { \
            Except_flag = Except_handled;

#define FINALLY \
        if (Except_flag == Except_entered) Except_stack = Except_stack->prev; \
        }{ \
           if (Except_flag == Except_entered) \
           Except_flag = Except_finalized;

#define END_TRY \
        if (Except_flag == Except_entered) Except_stack = Except_stack->prev; \
        } if (Except_flag == Except_raised) RERAISE; \
} while(0)

#undef T
#endif


/*
 * Checked runtime errors:
 * - To pass a null e to Except_raise
 *
 * Unchecked runtime errors:
 * - To execute C return statement inside a TRY-EXCEPT or TRY-FINALLY statement.
 *   If any of the statements in a TRY-EXCEPT or TRY-FINALLY must do a return,
 *   they must do so with RETURN macro. Switch statement is used in this macro
 *   so that both RETURN and RETURN e expand into one syntactically correct C statement.
 */

/*
 * First return in setjpm in TRY macro sets except flag to Except_entered,
 * which indicates that a try statement has been entered and
 * an exception frame has been pushed onto the exception stack.
 * Except_entered must be zero, because the initial call to setjmp returns zero;
 * subsequent returns from setjmp set it to Except_raised,
 * which indicates that an exception occurred.
 * Handlers set Except_flag to Except_handled to indicate that theyl've handled the exception.
 */